import { Component, OnInit } from '@angular/core';
declare let google: any;
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
            let data = google.visualization.arrayToDataTable([
                ['Task', 'Hours per Day'],
                ['Investment Grade', 5.2],
                ['High Yield Corporate', 1.3],
                ['High Yield Syndicated', 1.0],
                ['Bank Commercial & Industrial Loans', 2.1],
                ['Direct Lending - Government', 0.6],
                ['Direct Lending - Private Middle Market', 0.4]
            ]);

            let options = {
                title: '$10.6 Trillion U.S. Corporate Debt Market'
            };

            var chart = new google.visualization.PieChart(document.getElementById('piechart_div'))

            chart.draw(data, options);
        }
  }

}
